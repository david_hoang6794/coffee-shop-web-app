export interface PromotionBannerProps {
  image: string;
}
export interface PagingPromotionProps {
  dataBanner: PromotionBannerProps[];
}
export interface ItemSliderProps {
  image: string;
}
