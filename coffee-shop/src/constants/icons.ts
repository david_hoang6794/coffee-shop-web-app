import Logo from "../assets/icons/logo.png";
import AppStore from "../assets/images/app-store-badge.png";
import GoogleStore from "../assets/images/googlePlay.df026781.png";
import BocoIcon from "../assets/images/20150827110756-dadangky.png";
import NotFound from "../assets/images/not-found.png";
export { Logo, AppStore, GoogleStore, BocoIcon, NotFound };
