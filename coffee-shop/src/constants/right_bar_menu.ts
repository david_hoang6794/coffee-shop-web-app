import { RightBarItemData } from "../interface/right_bar_interface";

export const RIGHT_BAR_MENU: RightBarItemData[] = [
  {
    color: "red",
    title: "HANG MOI VE",
    price: "500.000 đ",
    id: 1,
    titleColor: "white",
    priceColor: "",
  },
  {
    id: 2,
    color: "green",
    title: "QUAN AO DEP",
    price: "300.000 đ",
    titleColor: "white",
    priceColor: "",
  },
  {
    id: 3,
    color: "blue",
    title: "PHO BIEN",
    price: "199.000 đ",
    titleColor: "white",
    priceColor: "",
  },
  {
    id: 4,
    color: "yellow",
    title: "PHOI DO DEP",
    price: "299.000 đ",
    titleColor: "black",
    priceColor: "",
  },
];
