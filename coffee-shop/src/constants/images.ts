import Banner from "../assets/images/banner.webp";
import Banner1 from "../assets/images/912x280-GDH-FIX.webp";
import Banner2 from "../assets/images/912x280-KCCEK.webp";
import Banner3 from "../assets/images/912x280-NEK-FIX.webp";
import Banner4 from "../assets/images/summer-home-912x280.webp";

const IMAGES = {
  sider_1: Banner,
  sider_2: Banner1,
  sider_3: Banner2,
  sider_4: Banner3,
  sider_5: Banner4,
};
export { IMAGES };
